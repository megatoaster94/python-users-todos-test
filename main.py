import os
import shutil
import sys
import requests
import json
import datetime
import tempfile


USERS_API = "https://jsonplaceholder.typicode.com/users"
TODOS_API = "https://jsonplaceholder.typicode.com/todos"


def mymkdir(directory):
    print("Making directory {0}".format(directory))
    try:
        os.makedirs(directory)
        print("done")
    except FileExistsError:
        print("directory exists!")
        pass
    except OSError:
        print ("Creation of the directory {0} failed".format(directory))
        sys.exit(1)


def mychdir(directory):
    old_path = os.getcwd()
    print("Changing directory to {0}".format(directory))
    try:
        os.chdir(directory)
        print("done")
    except FileNotFoundError:
        print("error")
        os.chdir(old_path)
        sys.exit(1)


def mymkchdir(directory):
    mymkdir(directory)
    mychdir(directory)


def main():
    mymkchdir("tasks")

    try:
        users_json_contents = requests.get(USERS_API).content
        todos_json_contents = requests.get(TODOS_API).content
    except requests.exceptions.RequestException as e:
        print(e)
        sys.exit(1)

    print("Users JSON contents:")
    print(users_json_contents)
    print("Todos JSON contents:")
    print(todos_json_contents)

    try:
        users_json_d = json.loads(users_json_contents)
        todos_json_d = json.loads(todos_json_contents)
    except ValueError:
        print("Invalid JSON file")
        sys.exit(1)

    for user in users_json_d:
        user_id = int(user["id"])
        user_name = str(user["name"])
        user_username = str(user["username"])
        user_email = str(user["email"])

        user_company_name = str(user["company"]["name"])

        user_completed_todos = []
        user_incompleted_todos = []

        for todo in todos_json_d:
            if user_id == int(todo["userId"]):
                todo_name = str(todo["title"])
                todo_name = ((todo_name)[:50] + '...') if len(todo_name) > 50 else todo_name
                #print(todo_name)
                if bool(todo["completed"]):
                    user_completed_todos.append(todo_name)
                else:
                    user_incompleted_todos.append(todo_name)

        filename = user_username + ".txt"

        now = datetime.datetime.now().strftime("%Y.%m.%d %H:%M")

        temp = tempfile.NamedTemporaryFile(mode='w')

        temp.write(user_name + ' <' + user_email + '> ' + now + '\n')
        temp.write(user_company_name + '\n')
        temp.write('\n')
        temp.write("Завершенные задачи:\n")
        for todo in user_completed_todos:
            temp.write(todo + '\n')
        temp.write('\n')
        temp.write("Оставшиеся задачи:\n")
        for todo in user_incompleted_todos:
            temp.write(todo + '\n')
        
        if os.path.isfile(filename):
            oldfile = open(filename)
            firstline = oldfile.readline()
            lastdate = firstline.split(sep="> ")[1].rstrip()
            #print(lastdate)
            date_time_obj = datetime.datetime.strptime(lastdate, "%Y.%m.%d %H:%M")
            oldfile.close()
            new_filename = user_username + date_time_obj.strftime("%Y-%m-%dT%H:%M") + ".txt"
            os.rename(filename, new_filename)

        temp.flush()
        shutil.copyfile(temp.name, filename)
        temp.close()


if __name__ == '__main__':
    main()
